n3t Debug integration
=====================

[n3t Debug][N3TDEBUG] is advanced debugging tool for Joomla, based on [Nette Tracy][TRACY].

n3tCookie Consent integrates into it by adding Consent info panel.

![n3t Debug integration](../images/usage/n3tdebug.jpg)

[N3TDEBUG]: https://n3t.bitbucket.io/
[TRACY]: https://tracy.nette.org/cs/
