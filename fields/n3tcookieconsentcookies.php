<?php
/**
 * @package n3t Cookie Consent
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2021- 2025 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

use Joomla\CMS\Form\Field\SubformField;
use Joomla\CMS\Form\FormHelper;
use Joomla\CMS\Language\Text;

defined( '_JEXEC' ) or die( 'Restricted access' );

FormHelper::loadFieldClass('subform');

class JFormFieldN3tCookieConsentCookies extends SubformField
{
	protected $type = 'N3tCookieConsentCookies';

	protected function getInput()
	{
		$input = parent::getInput();

		static $index = 0;
		$index++;

		$input = '<a href="#" data-toggle="collapse" data-target="#n3tCCCookies' . $index . '" data-bs-toggle="collapse" data-bs-target="#n3tCCCookies' . $index . '">' .
			'<i class="icon-eye"></i> ' . Text::_('PLG_SYSTEM_N3TCOOKIECONSENT_CFG_COLLAPSE_COOKIES') .
			'</a>' .
			'<div class="collapse show in" id="n3tCCCookies' . $index . '">' . $input . '</div>';
		return $input;
	}
}
