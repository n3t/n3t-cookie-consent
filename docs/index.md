n3t Cookie Consent
==================

![Version](https://img.shields.io/badge/Latest%20version-4.3.1-informational)
![Release date](https://img.shields.io/badge/Release%20date-2025--03--05-informational)

__A lightweight & gdpr compliant cookie consent solution for Joomla__

n3t Cookie Consent manager for [Joomla! CMS][JOOMLA] based on [Orestbida Cookie Consent][ORESTBIDA] javascript solution.

This is an out of box solution for Joomla! CMS, providing:

- Highly customizable Cookie Consent dialog
- Automatic detection of new Cookies
- Automatic cookies description based on [Open Cookie Database][OPENCOOKIEDATABASE]
- multi-lingual support
- multi-site / domain support
- block storing of any JavaScript Cookie until consent (but not blocking scripts itself, like
  Google Analytics, Google Tag Manager etc.)
- Logging of user consents
- ...and more

[JOOMLA]: https://www.joomla.org
[ORESTBIDA]: https://github.com/orestbida/cookieconsent
[OPENCOOKIEDATABASE]: https://github.com/jkwakman/Open-Cookie-Database
