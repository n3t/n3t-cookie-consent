Credits
=======

Thanks to these projects and contributors:

- [Orestbida][ORESTBIDA] for Cookie Consent javascript plugin
- [JWakman][OPENCOOKIEDATABASE] for Open Cookie Database
- [Joomla! CMS][Joomla]
- [Bongovo!][BONGOVO] for Czech and Slovak translation
- [Czech Joomla community][JOOMLAPORTAL] for testing, support and comments on development
- [Brian Teeman][BRIANTEEMAN] for English language corrections
- [Benno Mathias][BENNOMATHIAS] for German translation
- Luca Goldin for Italian translation
- [darekmer][DAREKMER] for Polish translation
- [Pip en El][PIPENL] for Dutch translation
- [Kenneth C Koppervik][WEBISTNO] for Norwegian translation
- Miroslav Ernst for Spanish translation
- Grégory R. for French Translation
- [Tansel Yuzer][SMARTSAILING] for Turkish Translation

[ORESTBIDA]: https://github.com/orestbida/cookieconsent
[OPENCOOKIEDATABASE]: https://github.com/jkwakman/Open-Cookie-Database
[JOOMLA]: https://www.joomla.org
[BONGOVO]: https://www.bongovo.cz/
[JOOMLAPORTAL]: https://www.joomlaportal.cz/
[BRIANTEEMAN]: https://brian.teeman.net/
[BENNOMATHIAS]: https://www.reisefotografien.eu/
[DAREKMER]: https://github.com/darekmer
[PIPENL]: https://pipnl.nl
[WEBISTNO]: https://webist.no
[SMARTSAILING]: https://smartsailing.org
