/**
 * @package n3t Cookie Consent
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2021 - 2025 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

(function(){
  'use strict';

  /**
   * @returns {Object} n3tConsentManager object
   */
  var n3tConsentManager = function(config) {
    /**
     * Singleton
     */
    if (window.n3tConsentManager)
      return window.n3tConsentManager;

    var _config = {
      'debug': false, // log debug info to console?
      'cookie_name': 'cc_cookie', // consent cookie name
      'cookie_report_name': null, // reporting cookie name
      'cookie_domain': window.location.hostname, // cookie domain
      'cookie_path': '/', // cookie path
      'cookie_domains': null, // set cookie for multiple domains
      'cookies': [], // list of cookies
      'options': {}, // options for cookieconsent
      'show_trigger': false, // Show the trigger
      'trigger_text': null, // text for trigger
      'trigger_icon': null, // icon for trigger
      'trigger_position': 'bottom left', // position of trigger top|nottom|middle left|right|center
      'trigger_layout': 'icon', // style of trigger icon|handle
      'tertiary_button_role': 'none', // tertiary button role - settings|accept_necessary
      'tertiary_button_text': null, // tertiary button text
      'tertiary_button_position': 'last', // tertiary button position - first|last
      'log_user_preferences_url': null, // URL for logging user consent
      'unknown_cookies': null, // how to handle unknown cookies accept|block|settings
      'consent_reload': false, // Reload the page after consent?
      'use_iframemanager': false, // Use IFrame manager
      'iframemanager_options': {} // IFrame manager options
    };

    if (typeof config === 'object')
      Object.assign(_config, config);

    /**
     * Holds Consent Manager object
     * @const {Object}
     */
    const _consentManager = {};

    /**
     * Holds IFrame Manager object
     * @const {Object}
     */
    let _iframeManager = {};

    /**
     * Holds Cookie Consent object
     * @const {Object}
     */
    let _cookieConsent = null;

    /**
     * Holds fake cookies (not passed to browser)
     * @var {array}
     */
    const fakeCookies = [];

    /**
     * Holds fake cookies (not passed to browser)
     * @var {array}
     */
    const fakeCookiesValues = [];

    /**
     * Helper function which prints info (console.log())
     * @param {string} print_msg
     * @param {Object} [optional_param]
     * @param {boolean} [error]
     */
    const _log = function(print_msg, optional_param, error) {
      print_msg = 'CookieConsent ' + print_msg;
      _config.debug && (!error ? console.log(print_msg, optional_param !== undefined ? optional_param : ' ') : console.error(print_msg, optional_param || ""));
    }

    /**
     * Helper function to generate UUID
     * inspired https://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid?page=1&tab=votes#tab-top
     */
    const _uuidv4 = function() {
      return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
      );
    }

    /**
     * override document.cookie property
     */
    const cookieDesc = Object.getOwnPropertyDescriptor(Document.prototype, 'cookie') ||
      Object.getOwnPropertyDescriptor(HTMLDocument.prototype, 'cookie');

    /**
     * Helper function - Sets the real Cookie and optionally logs
     * @param {string} val
     */
    const _setCookie = function(name, value) {
      if (value === undefined) {
        cookieDesc.set.call(document, name);
        _log('[SET]: ' + name);
      } else {
        cookieDesc.set.call(document, name + '=' + value);
        _log('[SET]: ' + name + '=' + value);
      }
    };

    /**
     * Helper function - Sets the real Cookie and optionally logs
     * @param {string} val
     */
    const _deleteCookie = function(name) {
      cookieDesc.set.call(document, name + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC');
      _log('[DELETE]: ' + name);
    };

    /**
     * Helper function - Sets the fake Cookie and optionally logs
     * @param {string} category
     * @param {string} val
     * @param {string} nameVal
     */
    const _blockCookie = function(category, val, nameVal) {
      _log('[BLOCKED] (' + category + '): ' + val);
      fakeCookiesValues.push(val);
      fakeCookies.push(nameVal);
    };

    /**
     * Helper function - Create reporting cookie with this cookie name, or add this cookie name to it.
     * @param {string} name
     */
    const _reportCookie = function(name) {
      if (!_config.cookie_report_name)
        return;

      _log('[REPORT] ' + name);

      var value = _getCookieValue(_config.cookie_report_name);
      if (value) {
        value = decodeURIComponent(value);
        value += ';';
      }
      _setCookie(_config.cookie_report_name, encodeURIComponent(value + name.trim())  + '; Domain=' + _config.cookie_domain + '; Path=' + _config.cookie_path);
    };

    /**
     * Helper function tries to set fake cookies again, based on current settings
     */
    const _updateFakeCookies = function() {
      fakeCookies.forEach(function(cookie) {
        document.cookie = cookie;
      });
    };

    /**
     * IFrameManager Helper function to get vimeo.com video thumbnail
     */
    _consentManager.ifmThumbvimeo = function(id, setThumbnail) {
      var url = "https://vimeo.com/api/v2/video/" + id + ".json";
      var xhttp = new XMLHttpRequest();

      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var src = JSON.parse(this.response)[0].thumbnail_large;
          setThumbnail(src);
        }
      };

      xhttp.open("GET", url, true);
      xhttp.send();
    }

    /**
     * IFrameManager Helper function to get dailymotion.com video thumbnail
     */
    _consentManager.ifmThumbdailymotion = function(id, setThumbnail) {
      var url = "https://api.dailymotion.com/video/" + id + "?fields=thumbnail_large_url";
      var xhttp = new XMLHttpRequest();

      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var src = JSON.parse(this.response).thumbnail_large_url;
          setThumbnail(src);
        }
      };

      xhttp.open("GET", url, true);
      xhttp.send();
    }

    if (cookieDesc && cookieDesc.configurable) {
      Object.defineProperty(document, 'cookie', {
        get: function () {
          let cookies = cookieDesc.get.call(document);
          if (fakeCookies.length) {
            if (_config.debug)
              cookies += '; __n3tFakeCookiesStartsHere=1';
            if (cookies !== '')
              cookies += '; ';
            cookies += fakeCookies.join('; ');
          }
          return cookies;
        },

        set: function (val) {
          let
            cookieVal = val.split(/;\s*/),
            nameVal = cookieVal[0],
            name = nameVal.split("=")[0],
            now = new Date(),
            cookieExpiration = null;

          if (name === _config.cookie_name)
            return _setCookie(val);

          cookieVal.forEach(function (value) {
            if (val.match(/^\s*expires=/i)) {
              try {
                cookieExpiration = Date.parse(value.split("=")[0]);
              } catch (e) {
                _log('[ERROR]', e, true);
              }
            }
          });

          if (cookieExpiration && (cookieExpiration < now))
            return _setCookie(val);

          const cookie = _findCookie(name);

          if (cookie && (cookie.required || _consentManager.allowedCategory(cookie.category)))
            return _setCookie(val);

          if (cookie)
            _blockCookie('category ' + cookie.category, val, nameVal);
          else {
            if (_config.unknown_cookies === 'accept' || (_config.unknown_cookies === 'settings' && _consentManager.allowedCategory('unknown')))
              _setCookie(val);
            else
              _blockCookie('unknown', val, nameVal);
            _reportCookie(name);
          }
        }
      });
    } else
      _log('[ERROR]: document.cookie property is not configurable', {}, true);

    /**
     * create UI
     */
    window.addEventListener('load', function () {
      if (_config.use_iframemanager) {
        _iframeManager = iframemanager();
        _consentManager.iframeManager = _iframeManager;

        const options = _config.iframemanager_options;
        for (const serviceName in options.services) {
          const service = options.services[serviceName];
          if (_consentManager['ifmThumb' + serviceName])
            options.services[serviceName].thumbnailUrl = _consentManager['ifmThumb' + serviceName];
        };
        _iframeManager.run(options);
      }

      if (_config.show_trigger) {
        let trigger = document.createElement('a');
        trigger.href = '#';
        trigger.ariaLabel = _config.trigger_text;
        trigger.innerHTML = _config.trigger_icon + '<span>' + _config.trigger_text + '</span>';
        trigger.className = 'cc-trigger cc-trigger-' + _config.trigger_layout + ' cc-trigger-' + _config.trigger_position.replace(/\s+/, ' cc-trigger-');
        trigger.addEventListener('click', function(e) {
          e.preventDefault();
          _cookieConsent.showSettings();
        });
        document.getElementsByTagName("body")[0].appendChild(trigger);
      }

      _cookieConsent = initCookieConsent();
      _consentManager.cookieConsent = _cookieConsent;

      let options = _config.options;
      // TODO take cookies from options to reduce code size
      options.cookie_name = _config.cookie_name;

      options.onFirstAction = function(user_preferences, cookie) {
        _log('[onFirstAction]');
        _sendPreferences();
        _updateFakeCookies();
        const event = new CustomEvent('n3t.cookieconsent.first_action', {
          detail: {
            consentManager: _consentManager,
            user_preferences: user_preferences,
            cookie: cookie,
          }
        });
        window.dispatchEvent(event);
        if (_config.consent_reload)
          window.history.go(0);
      };

      options.onAccept = function(cookie) {
        _log('[onAccept]');
        document.documentElement.classList.add('n3tcc-has-consent');

        if (_cookieConsent.get('data') && _cookieConsent.get('data').date && _cookieConsent.get('data').guid) {
          var consentDate = new Date(_cookieConsent.get('data').date).toLocaleString(),
            consentGUID = _cookieConsent.get('data').guid;

          _cookieConsent.getConfig('languages').default.settings_modal.blocks.forEach(block => {
            block.description = block.description.replace('{{consent_date}}', consentDate);
            block.description = block.description.replace('{{consent_guid}}', consentGUID);
          });
        } else {
          _cookieConsent.getConfig('languages').default.settings_modal.blocks.forEach(block => {
            if (block.description.includes('{{consent_date}}')
            || block.description.includes('{{consent_date}}')) {
              block.description = '';
            }
          });
        }

        setTimeout(function() {
          _cookieConsent.updateLanguage('default', true);
        }, 10);

        const event = new CustomEvent('n3t.cookieconsent.accept', {
          detail: {
            consentManager: _consentManager,
            cookie: cookie,
          }
        });
        window.dispatchEvent(event);
      };

      options.onChange = function(cookie, changed_categories) {
        _log('[onChange]');
        _sendPreferences();
        _updateFakeCookies();
        const event = new CustomEvent('n3t.cookieconsent.change', {
          detail: {
            consentManager: _consentManager,
            cookie: cookie,
            changed_categories: changed_categories
          }
        });
        window.dispatchEvent(event);
        if (_config.consent_reload)
          window.history.go(0);
      };

      _cookieConsent.run(options);

      /**
       * Tertiary button support
       */
      if (_config.tertiary_button_role !== 'none') {
        let consent_tertiary_btn = document.createElement('button');
        consent_tertiary_btn.type = 'button';
        consent_tertiary_btn.id = 'c-t-bn';
        consent_tertiary_btn.className = "c-bn c_link";
        consent_tertiary_btn.innerHTML = _config.tertiary_button_text;
        consent_tertiary_btn.addEventListener('click', function(e) {
          e.preventDefault();
          if (_config.tertiary_button_role === 'settings') {
            _cookieConsent.showSettings();
          } else {
            _cookieConsent.hide();
            _cookieConsent.accept([]);
          }
        });

        let elem = document.getElementById('c-bns');
        if (elem) {
          if (_config.tertiary_button_position === 'first')
            elem.insertAdjacentElement('afterbegin', consent_tertiary_btn);
          else
            elem.appendChild(consent_tertiary_btn);

          elem = document.getElementById('cm');
          elem.classList.add('has-tertiary-button');
        }
      }

      /**
       * Resolve triggers by classname
       */
      var elements = document.getElementsByClassName('n3tcc-settings');
      for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', function(e) {
          e.preventDefault();
          _cookieConsent.showSettings();
        });
      }

      elements = document.getElementsByClassName('n3tcc-scan');
      for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', function(e) {
          e.preventDefault();
          window.parent.postMessage('n3t_cookie_consent_finish_scan', '*');
        });
      }

      elements = document.getElementsByClassName('c-bn-scan');
      for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', function(e) {
          e.preventDefault();
          window.parent.postMessage('n3t_cookie_consent_finish_scan', '*');
        });
      }

      /**
       * Scan mode, report all cookies
       */
      var url = window.location.toString();
      if (_config.cookie_report_name && url.match(/[?&]n3tcc_scan=/)) {
        _reportCookie(_config.cookie_name);
        const cookies = document.cookie.split(';');
        cookies.forEach(function(cookie) {
          cookie = cookie.split('=');
          _reportCookie(cookie[0]);
        });
        window.location = url
          .replace(/[?&]n3tcc_scan=[^&#]*(#.*)?$/, '$1')
          .replace(/([?&])n3tcc_scan=[^&]*&/, '$1');
      }

      setTimeout(function() {
        document.documentElement.classList.add('n3tcc--loaded');
      }, 100);

      /**
       * Trigger loaded event
       */
      const event = new CustomEvent('n3t.cookieconsent.loaded', {
        detail: {
          consentManager: _consentManager
        }
      });
      window.dispatchEvent(event);
    });

    /**
     * Helper function sends AJAX request with user preferences (for Logging purposes)
     */
    const _sendPreferences = function() {
      var guid = _uuidv4(),
        now = new Date();
      _cookieConsent.set('data', {value: {guid: guid, date: now.toUTCString()}, mode: 'update'});

      const cookie = _findCookie(name);
      if (cookie && (cookie.required || _consentManager.allowedCategory(cookie.category)))
        return _setCookie(val);

      if (_config.log_user_preferences_url) {
        const xhttp = new XMLHttpRequest();
        xhttp.open('GET', _config.log_user_preferences_url, true);
        xhttp.send();
      }

      /**
       * Clear hidden cookies
       */
      const cookies = document.cookie.split(';');
      cookies.forEach(function(cookie) {
        cookie = cookie.split('=')[0].trim();
        if (!_cookieAllowed(cookie))
          _deleteCookie(cookie);
      });

      /**
       * Multidomain support
       */
      if (_config.cookie_domains) {
        _config.cookie_domains.forEach(function(value) {
          _log('[MultiDomainCookie]', value);
          let image = document.createElement('img');
          image.src = value + '&val=' + _getCookieValue(_config.cookie_name);
          image.style = "display:none";
          document.getElementsByTagName("body")[0].appendChild(image);
        });
      }
    }

    /**
     * Get cookie value
     * @returns {String}
     */
    const _getCookieValue = function(name) {
      var found = document.cookie.match("(^|;)\\s*" + name + "\\s*=\\s*([^;]+)");
      found = found ? found.pop() : "";
      return found;
    }

    /**
     * Get cookie consent value
     * @returns {Object}
     */
    const _getCookie = function() {
      var found = _getCookieValue(_config.cookie_name);
      if(found) {
        try {
          found = JSON.parse(found);
        } catch(e) {
          try {
            found = JSON.parse(decodeURIComponent(found))
          } catch (e) {
            found = {};
          }
        }
      } else
        found = {};

      return found;
    }

    /**
     * Returns array containing info about Cookie, if found, otherwise null
     * @param {string} cookieName
     * @returns {Object}
     */
    const _findCookie = function(cookieName) {
      return _config.cookies.find(function(cookie) {
        if (cookie.is_regex) {
          var re = new RegExp(cookie.name);
          return cookieName.match(re);
        } else
          return cookie.name === cookieName;
      });
    }

    /**
     * Returns true if cookie category is accepted by the user
     * @param {string} cookie_category
     * @returns {boolean}
     */

    const _allowedCategory = function(cookie_category) {
      return (_getCookie()['level'] || []).includes(cookie_category);
    }
    _consentManager.allowedCategory = _allowedCategory;

    /**
     * Returns true if cookie is listed in definition list
     * @param {string} cookieName
     * @returns {boolean}
     */
    const _cookieListed = function(cookieName) {
      return !!_findCookie(cookieName);
    }
    _consentManager.cookieListed = _cookieListed;

    /**
     * Returns true if cookie category is accepted by the user
     * @param {string} cookieName
     * @returns {boolean}
     */
    const _cookieAllowed = function(cookieName) {
      if (cookieName === _config.cookie_name)
        return true;

      var cookie = _findCookie(cookieName);
      if (cookie && (cookie.required || _consentManager.allowedCategory(cookie.category)))
        return true;
      if (_config.unknown_cookies === 'accept' || (_config.unknown_cookies === 'settings' && _consentManager.allowedCategory('unknown')))
        return true;

      return false;
    }
    _consentManager.cookieAllowed = _cookieAllowed;

    window.n3tConsentManager = _consentManager;
    return _consentManager;
  }

  /**
   * Make n3tConsentManager object accessible globally
   */
  if(typeof window['initN3tConsentManager'] !== 'function'){
    window['initN3tConsentManager'] = n3tConsentManager
  }
})();
