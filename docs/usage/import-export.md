Import / Export
===============

There are options to export / import cookies settings directly in plugin configuration.

Export settings
---------------

Using this button you are able to export complete plugin settings in JSON format. 
This could be useful for backups, or for transfer settings to other site.


Import settings
---------------

Import settings exported from n3t Cookie Consent plugin or from Cookiebot scan.

!!! warning "Importing can overwrite your current settings"
    By importing settings file you could loose all, or some of your current settings. 
    Be carefull when doing so, and always take **backup** of settings before import.

Export consents log
-------------------

Download current consent log. Note that consent logs could be downloaded also directly from your Joomla installation
using FTP,FTPS, SFTP or any other access you have to your Joomla instance.
