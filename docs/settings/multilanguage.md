Multi language setup
====================

n3t Cookie Consent fully supports multilingual sites. When multilanguage site is detected, 
small changes will appear in plugin settings:

Privacy policy
--------------

If privacy policy link is set by menu item, alternative languages privacy policies will be automatically detected from 
this menu item associations. 

If privacy policy is defined by URL, and multilanguage is enabled on Joomla, additional field `Alternate privacy policy URLs`
will appear in plugin settings, where you can specify different URLs for different languages.

Cookies definition
------------------

- If Expiration column is used, in cookies definition this will appear as two separated
  columns, one for expiration itself, and one for its unit (days, months, years, etc.)  
- Automatically detected cookies will have its description specified by language constant.
- When provider column is used, automatically detected cookies provided by site itself will have in its
  provider column pseudo language constant `SITENAME`, which is being replaced by site name value from 
  Global Joomla configuration.

### Custom cookies

Description, Provider and Expiration values for Cookies are translated by standard Joomla language constants, 
means, you can easily override it, or create new directly in your Joomla administration. 

