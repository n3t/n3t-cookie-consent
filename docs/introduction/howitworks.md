How does it work
================

n3t Cookie Consent blocks javascript functions to set Cookies. This means, if any script on your site
(such as Google Analytics script, Google Tag Manager, Facebook remarketing scripts, Google Ads scripts 
and generally any other) tries to set a Cookie, it is blocked. But not really blocked, it is just not 
written to disk. 

This means those scripts will work as expected, but will not store the tracking cookies. In other words, 
they will not be able to track the user between clicks on your pages.  

The n3t Cookie Consent script is loaded in such a way that it can block all scripts on site, even those 
written directly in a template, or added in template settings. There is no need to modify any of those 
scripts, everything works automatically.

An attractive informational dialog is displayed to users, they can choose to Accept / Reject settings, or 
view detailed settings to choose specific categories they accept.

![Consent dialog](../images/introduction/consent-dialog.jpg)

![Settings dialog](../images/introduction/settings-dialog.jpg)
