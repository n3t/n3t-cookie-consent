Page Scripts
============

n3t Cookies Consent by default handle Cookies, not Scripts. So, if you use for example
Google Analytics on your page, script itself will be still loaded, but it will be blocked to set cookies for your domain.

If you wish to block some script completely, you need to follow these additional steps:

1) **Enable Page Scripts option**

You can find it at [Advanced tab](advanced.md).

2) **Modify your scripts output**

You need to change `type` attribute of your scripts to `text/plain` and add `data-cookiecategory` category containing 
proper category on which the script should be executed, when user provides his consent.

So, for example, script loading some analytics JS file  
```
<script src="analytics.js" defer></script>
```
will become
```
<script type="text/plain" data-cookiecategory="analytics" src="analytics.js" defer></script>
```

Same could be used for inline scripts, so for example
```
<script type="text/plain" data-cookiecategory="ads">
    console.log('"ads" category accepted');
</script>
```

Available categories
--------------------

### Predefined blocks

If you use predefined blocks, available script categories are:

- __Functional cookies__ - `functional`  
- __Preferences cookies__ - `preferences`
- __Analytics cookies__ - `analytics`
- __Marketing cookies__ - `marketing`
- __Unknown cookies__ - `unknown`

### Custom blocks

If you use custom blocks, category is specified by its Alias.
