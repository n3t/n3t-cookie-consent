Quick setup
===========

Install and enable the plugin
-----------------------------

Install the plugin using Joomla extension installer, same way as any other extension.
Then go to plugin manager and enable the plugin. 

Collect cookies
---------------

Open plugin settings and see page Cookies. First click on "Load defaults" button.
This will load definition of basic system Cookies your Joomla uses. Second click on 
"Scan site for cookies" button, which will open your frontend in "Scanning mode".

![Quick setup](../images/settings/quick-setup.jpg)

You should see already popup with Cookie Consent.
Accept all cookies, and start to navigate through your website. Open few pages, 
navigate through the site, use some actions (like add item to cart and checkout 
in eshop), if your site supports login then log in etc. Open pages where reCaptcha 
is placed (if you use it), where youtube video is placed (if you have any) etc.   

You can watch how Settings dialog is getting slowly filled by cookies. On every page you visit, 
you should see warning, that you are in cookies scaning mode, with link to end it. Not, that if this appears, 
and how depends on your template. The screenshot bellow is taken from Cassiopea template, which comes integrated in Joomla 4.

![Scanning mode](../images/settings/scan-mode.jpg)

If you don't see this, or similar warning, still you can open Cookie preferences, and end scanning mode there.

![Scanning mode dialog](../images/settings/scan-mode-dialog.jpg)

Review the settings
-------------------

After you are finished, your administration interface will appear again. 

You will se now all collected Cookies, categorized, with description, ready to use.
Just review your settings, change categories of some Cookies if you need to, save, 
and you ae ready to go.

Of course, if you want, go through detailed settings, like 
- [customize cookies](cookies.md)
- [appearence](appearance.md)
- or other...

Use CookieBot
-------------

Other way to quick setup could be to use free service of [CookieBot.com](https://www.cookiebot.com).
It offers free scan of website, which scan all used cookies. Do this with disabled n3t Cookie Consent plugin, 
so no Cookies are blocked. The report could be sent to your email in PDF format, which means 
you have to copy / paste it in n3t Cookie Consent Settings table, or, if you create free account there, 
could be exported in JSON format, and then [easily imported](../usage/import-export.md) in n3t Cookie Consent plugin.

Note that [CookieBot.com](https://www.cookiebot.com) service provide just english description of Cookies, 
in opposite to native n3t Cookie Consent scan, which provides translation based on your languge (if you have
installed correct [translation](../about/translations.md) of n3t Cookie Scan)

!!! note "CookieBot.com"
    [CookieBot.com](https://www.cookiebot.com) is an external service not connected any way with n3t Cookie Consent.
    If you meet troubles during generating report for your site, please contact directly them. 
