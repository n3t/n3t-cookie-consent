Basic settings
==============

### Privacy policy type
Choose if your privacy policy is a menu item (means you have menu item pointing your privacy policy) or direct link 
(for example if pointing privacy policy on other website).

### Privacy policy

Select menu item pointing to your privacy policy.

### Privacy policy URL

Enter full URL address (starting with http:// or https://) of privacy policy page.

### Log consents

Store log with user consents. Log file can be found in Joomla! logs directory.

### Rotate log

When enabled, log will be saved in standard logs path of Joomla, means influenced by 'Log rotate' plugin. When disabled, 
log will be saved in 'keep' subfolder of logs path, means, it will never get rotated.

### Revision

Specify this option to enable revisions. If you change something in your cookie policy, you should require new consent 
from users. Defining some number here will reset all given consents so far, and all users will have to review their 
choices again.
