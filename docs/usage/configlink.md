Link to open config dialog
==========================

If you do not want to use built-in trigger to open config dialog, you have few options, how to add 
link to your website, which opens Cookies settings dialog:

Content macro
-------------

Use built-in `{n3tcookieconsent settings}Text of the link{/n3tcookieconsent}` macro. 
If you place this macro in some article, custom content module or anywhere else, where standard Joomla `onContentPrepare`
event is used, it will get replaced by link to open Cookies settings dialog. Note that `Text of the link` do not need 
to be just text, but, for example, also image.

This is the recommended way to add settings trigger to articles and custom modules.

Link class
----------

Add `n3tcc-settings` to any link, or element, which, on click will open Cookies settings dialog.

This is the recommended way to add links to menu items. Just create menu item of type `External URL`,
use `#` as its link and add `n3tcc-settings` class to it. 

Custom HTML
-----------

Use following syntax to open Cookies settings dialog:

```
<button type="button" data-cc="c-settings">Show cookie settings</button>
```

Additional data-cc attributes could be used for following actions:

- `accept-all`: accept all categories
- `accept-necessary`: accept only categories marked as necessary/readonly (reject all)
- `accept-custom`: accept currently selected categories inside the settings modal

Custom Script
-------------

Consent manager variable is registered as `n3tConsentManager.cookieConsent` global JavaScript variable.
You can access it from your custom scripts and call its functions according to [Orestbida documentation][ORESTBIDA].

[ORESTBIDA]: https://github.com/orestbida/cookieconsent
