n3t Cookie Consent
==================

![Version](https://img.shields.io/badge/Latest%20version-4.3.1-informational)
![Release date](https://img.shields.io/badge/Release%20date-2025--03--05-informational)

[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-v3.10-green)][JOOMLA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-v4.x-green)][JOOMLA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-v5.x-green)][JOOMLA]
[![PHP](https://img.shields.io/badge/PHP-v7.4-green)][PHP]
[![Documentation Status](https://readthedocs.org/projects/n3t-cookie-consent/badge/?version=latest)](https://n3t-cookie-consent.readthedocs.io/en/latest/?badge=latest)

Joomla Cookie Consent solution
------------------------------

Cookie Consent Manager for [Joomla! CMS][JOOMLA] based on [Orestbida Cookie Consent][ORESTBIDA] javascript solution.

This is out of the box solution for Joomla! CMS, providing:
- Highly customizable Cookie Consent dialog
- Automatic detection of new Cookies and their
  description based on [Open Cookie Database][OPENCOOKIEDATABASE]
- multi lingual support
- multi site / domain support
- block storing of any JavaScript Cookie until consent (but not blocking scripts itself, like
  Google Analytics, Google Tag Manager etc.)
- Logging of user consents
- ...and more

[JOOMLA]: https://www.joomla.org
[PHP]: https://www.php.net
[ORESTBIDA]: https://github.com/orestbida/cookieconsent
[OPENCOOKIEDATABASE]: https://github.com/jkwakman/Open-Cookie-Database
