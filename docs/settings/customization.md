Customization
=============

n3t Cookie Consent support fully Joomla override system. This means, that you can easily override
any HTML code, CSS code and/or JavaScript code in your template.

HTML
----

Overridable HTML files are:

- `tmpl/script.php` - defines the initialization script for Cookie Consent. You can override very basic functionalities here. 
- `tmpl/style.php` - defines custom inline styles for consent dialogs. Could be overriden to add moreadvanced styles. 
- `tmpl/trigger.php` - defines SVG icon for trigger. Define your custom icon here.

!!! note "How to override HTML output in Joomla!"
    To override HTML file simply go to your template folder `templates/YOUR_TEMPLATE_NAME`, 
    create `html/plg_system_n3tcookieconsent` subfolder there, if not exists yet and copy original file from 
    `plugins/system/n3tcookieconsent/tmpl` into this new location. Then make your changes. This will make your changes 
    safe from overwriting during update.

CSS
---

Overridable CSS files are:

- `n3tconsentmanager.css` - styles for cookie consent manager.
- `n3tconsentmanager.min.css` - same as above, but minimised for production use.
- `iframemanager.css` - styles for iframe manager.
- `iframemanager.min.css` - same as above, but minimised for production use.

!!! note "How to override CSS files in Joomla! 3.x"
    To override CSS file simply go to your template folder `templates/YOUR_TEMPLATE_NAME`,
    create `css/plg_n3tcookieconsent` subfolder there, if not exists yet and copy original file from `media/plg_n3tcookieconsent/css`
    into this new location. Then make your changes. This will make your changes safe from overwriting during update.

!!! note "How to override CSS files in Joomla! 4.x"
    To override CSS file simply go to your template media folder `media/templates/site/YOUR_TEMPLATE_NAME`, 
    create `css/plg_n3tcookieconsent` subfolder there, if not exists yet and copy original file from `media/plg_n3tcookieconsent/css` 
    into this new location. Then make your changes. This will make your changes safe from overwriting during update.

If you are experienced with LESS, source less files could be found in `media/plg_n3tcookieconsent/less` folder.
 
JavaScript
----------

Overridable JS files are:

- `n3tconsentmanager.min.js` - this file initialise the cookie consent manager and loads settings to it.
- `cookieconsent.min.js` - this is Orestbida cookie manager itself.
- `iframemanager.min.js` - this file contains iFrame manager. 

!!! note "How to override JavaScript files in Joomla! 3.x"
    To override JavaScript file simply go to your template folder `templates/YOUR_TEMPLATE_NAME`,
    create `js/plg_n3tcookieconsent` subfolder there, if not exists yet and copy original file from `media/plg_n3tcookieconsent/js`
    into this new location. Then make your changes. This will make your changes safe from overwriting during update.

!!! note "How to override JavaScript files in Joomla! 4.x"
    To override JavaScript file simply go to your template media folder `media/templates/site/YOUR_TEMPLATE_NAME`,
    create `js/plg_n3tcookieconsent` subfolder there, if not exists yet and copy original file from `media/plg_n3tcookieconsent/js`
    into this new location. Then make your changes. This will make your changes safe from overwriting during update.
