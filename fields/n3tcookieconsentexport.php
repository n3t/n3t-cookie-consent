<?php
/**
 * @package n3t Cookie Consent
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2021- 2025 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

use Joomla\CMS\Form\FormField;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\Router\Route;

defined( '_JEXEC' ) or die( 'Restricted access' );

class JFormFieldN3tCookieConsentExport extends FormField
{
	protected $type = 'N3tCookieConsentExport';

	protected function getInput()
	{
		if (PluginHelper::isEnabled('system', 'n3tcookieconsent'))
			return '<a href="' . Route::_('index.php?option=com_ajax&group=system&format=raw&plugin=N3tCookieConsentExport') . '" download class="btn btn-primary" id="n3t-cookie-consent-export">' .
				'<i class="icon-download"></i> ' . Text::_('PLG_SYSTEM_N3TCOOKIECONSENT_CFG_EXPORT_BTN') .
				'</a>';

		return '<p class="alert alert-warning">' . Text::_('PLG_SYSTEM_N3TCOOKIECONSENT_CFG_ENABLE_PLUGIN_FIRST') . '</p>';
	}
}
