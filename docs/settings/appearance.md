Appeareance settings
====================

n3t Cookie Consent could be personalized in many ways.
Find bellow description of way, how to change appearance of Consent dialog, 
Cookie settings dialog and settings Trigger.

There are plenty options how to influence appearance already built-in in plugin settigns:

Consent dialog
--------------

These settings influence how the consent dialog will look like and some of its behaviors.

### Force consent

When this option is enabled, consent dialog will appear to user above overlaying layer, hiding the site itself.
This means, users have to make their decision about cookies first, before viewing and interacting with the site 
itself. This is usually required by marketing specialists, but lawyers doesn't like to see it, as the consent is not 
voluntary.

Default is `No`.

### Consent dialog layout

Choose consent dialog layout. It could be

#### Box

![Consent dialog - box](../images/settings/appearance-box.jpg)

#### Cloud

![Consent dialog - cloud](../images/settings/appearance-cloud.jpg)

#### Bar

![Consent dialog - bar](../images/settings/appearance-bar.jpg)

Default is `Box`.

### Consent dialog position

Here you can choose on-screen position, where consent dialog should appear.

Default is `bottom right`.

### Consent dialog transition

Choose transition effect, how consent dialog appears to user.

Default is `Slide`.

### Primary button role

Choose role of primary (highlighted) button.

- Accept all (immediately accept all cookies categories)
- Accept selected (immediately accept just 'always enabled' cookies categories)

Default is `Accept all`.

### Secondary button role

Choose role of secondary button.

- None (button is not displayed)
- Settings (settings dialog is displayed)
- Reject all (reject all except 'always enabled' cookies categories)

Default is `Settings`.

### Tertiary button role

Choose role of tertiary button.

- None (button is not displayed)
- Settings (settings dialog is displayed)
- Reject all (reject all except 'always enabled' cookies categories)

Default is `None`.

!!! note "Tertiary button"
    In default configuration, n3t Cookie Consent comes just with 2 buttons. Some lawyers requires 'Reject all' button, 
    if there is 'Accept all' button in consent dialog. If you are in such situation, you will propably need three buttons,
    one for Aceept all, one for Reject all and one for Settings.

### Swap buttons

When enabled, order of buttons is displayed opposite. Meaning primary button (the highlighted one) is disaplyed on the right.
According to some hot areas studies, this is good better position, mainly on mobile devices, to place button, which users click more easily. 

Default is `Yes`.

Settings dialog
---------------

These settings influence how the settings dialog will look like and some of its behaviors.

### Settings dialog layout

Choose settings dialog layout. It could be

#### Bar

![Settings dialog - bar](../images/settings/settings-bar.jpg)

#### Box

![Settings dialog - box](../images/settings/settings-box.jpg)

Default is `Bar`.

### Settings dialog position

Here you can choose on-screen position, where settings dialog should appear. Note that this only influence Bar layout.

Default is `Right`.

### Settings dialog transition

Choose transition effect, how settings dialog appears to user.

Default is `Slide`.

### Show Reject all

Show or hide 'Reject all' button in settings dialog.

Default is `Yes`.

### Hide cookies tables

When this is enabled, list of cookies is not presented to users. They will see just categories, and its descriptions, but not 
list of cookies itself.

Default is `No`.

### Show Cookie Provider column

When enabled, additional column for Cookie provider will be displayed per cookie.

Default is `Yes`.

### Show Cookie Expiration column

When enabled, additional column for Cookie expiration will be displayed per cookie.

Default is `Yes`.

Trigger
-------

These settings influence how the settings trigger will look like and some of its behaviors.

### Show Trigger

### Trigger layout

Choose settings trigger layout layout. It could be

#### Icon

![Settings dialog - bar](../images/settings/trigger-icon.jpg)

#### Handle

![Settings dialog - box](../images/settings/trigger-handle.jpg)

Default is `Icon`.

### Trigger position

Here you can choose on-screen position, where consent dialog should appear. Note that this only influence Bar layout.

Default is `Bottom left`.

Colours
-------

Here you can set many colour options. Please play with it and try yourself. 

Custom style
------------

At expert tab in plugin settings you can also add custom CSS styling. Rules defined here will be included in rendered document.