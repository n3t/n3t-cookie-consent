License
=======

n3t Cookie Consent is released under [GNU/GPL v3][GNUGPL] license.

[GNUGPL]: http://www.gnu.org/licenses/gpl-3.0.html