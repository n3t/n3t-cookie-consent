Advanced settings
=================

### Disallow tmpl

List of `tmpl` parameters, where Cookie Consent should not be displayed.  
`component` and `raw` by default.

### Hide from bots

Enable if you don't want the plugin to run when a bot/crawler/webdriver is detected.
Make search crawlers do not index content of your Consent dialog instead of 
your real content.

### Cookie name

Name of the cookie used to store consent. If empty 'n3t_cc' will be used.

### Cookie SameSite attribute

[SameSite](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite) attribute for Cookie.


### Cookie domain

Specify your domain (will be grabbed by default) or a subdomain. If empty, value from Joomla global configuration is used.
Usually do not need to be specified.

### Multiple domains

If you want to share consent through multiple domains, specify those here. Find more information at 
[separate documentation page](multidomain.md).

### Cookie path

Path where the cookie will be set. If empty, value from Joomla global configuration is used.
Usually do not need to be specified.

### Cookie expiration

Number of days before the cookie with consent expires (182 days = 6 months, 395 days = 13 months).

### Cookie rejected expiration

Number of days before the cookie with consent expires, if user rejects non-essential cookies (182 days = 6 months, 395 days = 13 months).
If empty (default), the lifetime of consent cookie will be the same, regardless user rejects, or accepts cookies.

### Page scripts

Enable if you want to easily manage script tags. Find more information at
[separate documentation page](pagescripts.md).

### Autorun

If enabled, show the cookie consent as soon as possible (otherwise you need to manually call the .show() method from 
your template or other script).

### Delay

Specify number of milliseconds before showing the consent-modal.

### Reload page on consent

If enabled, whenever user changes his consent, the whole page will be reloaded in browser.
This could be useful for more sophisticated solutions, which relies on cookies on server side.
