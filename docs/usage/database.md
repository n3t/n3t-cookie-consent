Cookies Database
================

n3t Cookie Consent comes with two integrated database of cookies.
These databases are used to auto-generate description, providers and expiration of 
auto-detected cookies.

Internal Joomla Database
------------------------

Database of cookies used by Joomla core, and some other extensions.

Open Cookie Database
--------------------

[Open Cookie Database][OPENCOOKIEDATABASE] is Open source project maintained by JWakman at GitHub.
It is an effort to describe and categorise all major cookies.

[OPENCOOKIEDATABASE]: https://github.com/jkwakman/Open-Cookie-Database
