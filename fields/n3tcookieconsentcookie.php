<?php
/**
 * @package n3t Cookie Consent
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2021- 2025 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

use Joomla\CMS\Form\Field\TextField;
use Joomla\CMS\Form\FormHelper;
use Joomla\CMS\Version;

defined( '_JEXEC' ) or die( 'Restricted access' );

if (Version::MAJOR_VERSION === 3) {
	FormHelper::loadFieldClass('text');
	class_alias('\\JFormFieldText', '\\Joomla\\CMS\\Form\\Field\\TextField');
}

class JFormFieldN3tCookieConsentCookie extends TextField
{
	protected $type = 'N3tCookieConsentCookie';

	protected function getInput()
	{
		$input = parent::getInput();

		$input = '<div class="input-group flex-nowrap input-append">' . $input .
			// space after blank is because of Joomla 4 default template adding icon to _blank links
			'<a href="#" class="btn btn-info" target="_blank " onclick="this.href=\'https://cookiedatabase.org/?s=\'+this.previousElementSibling.value.replace(/^\^/, \'\');"><i class="icon-help"></i></a>' .
			'</div>';
		return $input;
	}
}
