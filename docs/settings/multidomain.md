Multi domain setup
==================

If you want to share users Cookie Consent between multiple domains (like en.example.com / cz.example.com or exmaple.com / example.cz)
you have two options:

Subdomains
----------

If you need to share cookie consent between subdomains (like en.example.com, cz.example.com, demo.example.com etc.),
you just need to specify Cookie domain (and Cookie path, if necessary) in [advanced settings](advanced.md).

In such case, you just need to specify for which domain the cookie should be valid. So for example value of `.example.com`
will cover all subdomains of example.com domain, including for example www.example.com, en.example.com, cs.example.com or demo.example.com.

This doesn't need to be done, if you set this already in global Joomla configuration.

Multiple domains
----------------

If you use multiple domains (like example.com, example.cz, otherexample.com etc.) you need to specify all of these domains 
in Multiple domains field in [advanced settings](advanced.md).

!!! warning "Multiple domains"
    Note that this setup will work ONLY if all of specified domains are running on secure https:// protocol.

Setup
-----

In both cases on all involved domains has to be of course installed n3t Cookie Consent, with completely same setup. 
If you use some multi domain plugin (like [n3t Language domains][N3TLANGUAGEDOMAINS] for example), you don't need to care, as all domains are running 
on same Joomla instance (database), so you have installed just one instance of plugin.

If you share Cookie Consent between multiple Joomla instances, you can use [Export / Import](../usage/import-export.md) function to share the settings.

[N3TLANGUAGEDOMAINS]: https://n3t.bitbucket.io/extension/n3t-language-domains
