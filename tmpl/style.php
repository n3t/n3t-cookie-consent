<?php
/**
 * @package n3t Cookie Consent
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2021 - 2025 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );
/**
 * @var array $displayData
 * @var array $styles
 * @var \Joomla\Registry\Registry $params
**/
extract($displayData);

if (!$styles && !$params->get('custom_style'))
  return;
?>
<?php if (false) { ?><style><?php } // Just to make code highlighting working correctly ?>
<?php if ($styles) { ?>
:root {
  <?php foreach ($styles as $name => $value) { ?>
  <?php echo $name; ?>: <?php echo $value; ?>;
  <?php } ?>
}
<?php } ?>
<?php echo $params->get('custom_style'); ?>
<?php if (false) { ?></style><?php } // Just to make code highlighting working correctly
