<?php
/**
 * @package n3t Cookie Consent
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2021 - 2025 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Factory;
use Joomla\CMS\Installer\InstallerScript;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Version;
use Joomla\Registry\Registry;

class plgSystemN3tCookieConsentInstallerScript extends InstallerScript
{
	const LANG_PREFIX = 'PLG_SYSTEM_N3TCOOKIECONSENT';
	protected $minimumJoomla = '3.10.0';
	protected $minimumPhp = '7.2.0';
	protected $allowDowngrades = true;
	protected $oldRelease = null;

	/**
	 * @inheritdoc
	 *
	 * @since 4.0.0
	 */
	public function preflight($type, $parent)
	{
		$return = parent::preflight($type, $parent);

		if (strtolower($type) === 'update') {
			if (Version::MAJOR_VERSION === 3) {
				$db = Factory::getDbo();
				$manifest = $this->getItemArray('manifest_cache', '#__extensions', 'name', $db->quote($this->extension));
			} else {
				$manifest = $this->getItemArray('manifest_cache', '#__extensions', 'name', $this->extension);
			}

			if (isset($manifest['version'])) {
				$this->oldRelease = $manifest['version'];
			}
		}
		return $return;
	}

	/**
	 * Store default plugin parameters
	 * @param  \stdClass $parent - Parent object calling object.
	 *
	 * @since 4.0.0
	 */
	private function storeDefaultParams($parent)
	{
		$params = new Registry();

		$blocks = [];
		$blocks[] = (object)['type' => 'description'];
		$blocks[] = (object)['type' => 'functional'];
		$blocks[] = (object)['type' => 'preferences'];
		$blocks[] = (object)['type' => 'analytics'];
		$blocks[] = (object)['type' => 'marketing'];
		$blocks[] = (object)['type' => 'unknown'];
		$blocks[] = (object)['type' => 'hidden'];
		$blocks[] = (object)['type' => 'system'];
		$blocks[] = (object)['type' => 'privacy'];
		$blocks[] = (object)['type' => 'consent'];
		$params->set('blocks', $blocks);

		$db = Factory::getDbo();
		$query = $db->getQuery(true);
		$query->update('#__extensions')
			->set('params=' . $db->quote((string)$params))
			->where($db->quoteName('name') . ' = ' . $db->quote($parent->getName()));
		$db->setQuery($query)->execute();
	}

	/**
	 * @inheritdoc
	 *
	 * @since 4.0.0
	 */
	public function postflight($type, $parent)
	{
		if ($type === 'install') {
			$this->storeDefaultParams($parent);
		}
	}

	/**
	 * @inheritdoc
	 *
	 * @since 4.0.0
	 */
	public function update($parent)
	{
		if ($this->oldRelease) {
			Factory::getApplication()->enqueueMessage(Text::sprintf(self::LANG_PREFIX . '_INSTALL_UPDATE_VERSION', $this->oldRelease, $this->release));
		}
	}

}
