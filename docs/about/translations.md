Translations
============

List of known translations by community: 

- __[![cs-cz](../images/flags/cz.png) Czech translation][CS_CZ]__ by [Bongovo!][BONGOVO]
- __[![de-de](../images/flags/de.png) German translation][DE_DE]__ by [Benno Mathias][BENNOMATHIAS]
- __[![es-es](../images/flags/es.png) Spanish translation][ES_ES]__ by Miroslav Ernst
- __[![fr-fr](../images/flags/fr.png) French translation][FR_FR]__ by Grégory R.
- __[![it-it](../images/flags/it.png) Italian translation][IT_IT]__ by Luca Goldin
- __[![nb-no](../images/flags/no.png) Norwegian translation][NB_NO]__ by [Kenneth C Koppervik][WEBISTNO] 
- __[![sk-sk](../images/flags/nl.png) Dutch translation][NL_NL]__ by [Pip en El][PIPENL] 
- __[![pl-pl](../images/flags/pl.png) Polish translation][PL_PL]__ by [darekmer][DAREKMER]
- __[![sk-sk](../images/flags/sk.png) Slovak translation][SK_SK]__ by [Bongovo!][BONGOVO]
- __[![tr-tr](../images/flags/tr.png) Turkish translation][TR_TR]__ by [Tansel Yuzer][SMARTSAILING]

!!! info "Translations"
    Translations listed here are not official part of n3t Cookie Consent plugin.
    It is created and maintained by volunteers. For any info about translation, bug reports
    etc. contact directly their authors.

[CS_CZ]: https://www.bongovo.cz/ke-stazeni/category/232-system-n3t-cookie-consent
[DE_DE]: https://www.reisefotografien.eu/downloads/category/222-n3t-cookie-consent
[ES_ES]: https://bitbucket.org/n3t/n3t-cookie-consent/downloads/es-ES.plg_system_n3tcookieconsent.4.1.0.zip
[FR_FR]: https://bitbucket.org/n3t/n3t-cookie-consent/downloads/fr-FR.plg_system_n3tcookieconsent.4.2.0.zip
[IT_IT]: https://bitbucket.org/n3t/n3t-cookie-consent/downloads/it-IT.plg_system_n3tcookieconsent.4.1.0.zip
[NB_NO]: https://bitbucket.org/n3t/n3t-cookie-consent/downloads/no-NO.plg_system_n3tcookieconsent.4.0.0.zip
[NL_NL]: https://pipnl.nl/downloads/nl-NL.plg_system_n3tcookieconsent_j4.zip
[PL_PL]: https://github.com/darekmer/translation-n3t-Cookie-Consent/tags
[SK_SK]: https://www.bongovo.cz/ke-stazeni/category/232-system-n3t-cookie-consent
[TR_TR]: https://bitbucket.org/n3t/n3t-cookie-consent/downloads/tr-TR.plg_system_n3tcookieconsent-j3-j4.zip


[BONGOVO]: https://www.bongovo.cz/
[BENNOMATHIAS]: https://www.reisefotografien.eu/
[DAREKMER]: https://github.com/darekmer
[PIPENL]: https://pipnl.nl
[WEBISTNO]: https://webist.no
[SMARTSAILING]: https://smartsailing.org
