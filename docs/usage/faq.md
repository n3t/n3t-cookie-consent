FAQ
===

How to implement Google Consent Mode?
-------------------------------------

Google supports Consent mode using gtag. As n3t Cookie Consent doesn't allow to store any cookies, until
consent is given by user, it is not necessary to use it. However, if you want to apply it, you will need to add
some extra scripts in n3t Cookie Consent plugin settings on page Expert.

First add an Initialize script
```  
  window.dataLayer = window.dataLayer || [];  
  function gtag(){dataLayer.push(arguments);}
  gtag('consent', 'default', {
    'ad_storage': 'denied',
    'analytics_storage': 'denied',
    'wait_for_update': 1000    
  });
```
This script will run before any other script, so will predefine values for dataLayer.
Just check code you use to integrate Google Analytics, that it use syntax like 
```  
  window.dataLayer = window.dataLayer || [];  
```
and not directly something like 
```  
  window.dataLayer = [];  
```

Second, add this script to Accept and Change events in n3t Cookie Consent settings
```  
  gtag('consent', 'update', {
    'ad_storage': n3tConsentManager.allowedCategory('marketing') ? 'granted' : 'denied',
    'analytics_storage': n3tConsentManager.allowedCategory('analytics') ? 'granted' : 'denied',
  });
```
This will work, if you use predefined 'Functional' and 'Marketing' blocks. If you use custom blocks, just replace 
the 'marketing' and 'analytics' keywords with an aliases of your blocks.

!!! info "Google Consent v2"
    In 2024 Google introduced Updates to consent mode for traffic in European Economic Area (EEA).
    Two new agreements `ad_user_data` and `ad_personalization` need to be sent to Google.
    If your site is affected, please use following instructions.

To follow this new regulation, you need to modify above scripts as follows:

Initialize script:
```  
  window.dataLayer = window.dataLayer || [];  
  function gtag(){dataLayer.push(arguments);}
  gtag('consent', 'default', {
    'ad_storage': 'denied',
    'ad_user_data': 'denied',
    'ad_personalization': 'denied',
    'analytics_storage': 'denied',
    'wait_for_update': 1000    
  });
```

Accept and Change scripts:
```  
  gtag('consent', 'update', {
    'ad_storage': n3tConsentManager.allowedCategory('marketing') ? 'granted' : 'denied',
    'ad_user_data': n3tConsentManager.allowedCategory('marketing') ? 'granted' : 'denied',
    'ad_personalization': n3tConsentManager.allowedCategory('marketing') ? 'granted' : 'denied',
    'analytics_storage': n3tConsentManager.allowedCategory('analytics') ? 'granted' : 'denied',
  });
```

This configuration will send both new required agreements based on users choice in predefined `Marketing` section.

If you want to give user more options to decide, and be more granular in his decision, you need to create 2 new custom sections
in you Cookies definition, and modify the above code based on these sections. So for example, if you create 2 new sections 
with aliases `ad_user_data` and `ad_personalization`, the code would look like this:

```  
  gtag('consent', 'update', {
    'ad_storage': n3tConsentManager.allowedCategory('marketing') ? 'granted' : 'denied',
    'ad_user_data': n3tConsentManager.allowedCategory('ad_user_data') ? 'granted' : 'denied',
    'ad_personalization': n3tConsentManager.allowedCategory('ad_personalization') ? 'granted' : 'denied',
    'analytics_storage': n3tConsentManager.allowedCategory('analytics') ? 'granted' : 'denied',
  });
```

Sklik conversion and retargeting code
-------------------------------------

If you use [Sklik conversion code](https://napoveda.sklik.cz/merici-skripty/konverzni-kod/) or
[Sklik retargteting code](https://napoveda.sklik.cz/merici-skripty/retargetingovy-kod/)
you should modify its code to add consent info. Using n3t Cookie Consent this would look like something
like this:

```
<script src="https://c.seznam.cz/js/rc.js"></script>
<script>
    window.sznIVA.IS.updateIdentities({  
        eid: "email@example.com",
        aid: {
            "a1":"Česká republika",
            "a2":"Praha",
            "a3":"Radlická", 
            "a4":"3294/10", 
            "a5":"150 00", 
        },
        tid: "+420 234 694 111"
    });

    window.addEventListener('n3t.cookieconsent.accept', function(event) {
      /* retargeting */
      var retargetingConf = {
          rtgId: 123456, 
          itemId: "67890", 
          category: "Zahrada | Stínící technika | Zahradní slunečníky",
          pageType: "offerdetail",
          rtgUrl: "https://example.com?hello=world", 
          consent: n3tConsentManager.allowedCategory('marketing') ? 1 : 0, 
      };
  
     window.rc.retargetingHit(retargetingConf);
     
     /* Conversion */
      var conversionConf = {
          id: 10000000, 
          value: 199.9, 
          orderId: 123456, 
          zboziType: "standard", 
          zboziId: "QQQQ", 
          consent: n3tConsentManager.allowedCategory('marketing') ? 1 : 0,
      };
  
      window.rc.conversionHit(conversionConf);   
    });
</script>  
```

How to change trigger icon?
---------------------------

Trigger icon could be changed using [override system](../settings/customization.md) directly in your template.

How to open settings dialog from menu?
--------------------------------------

There are few options how to achieve this, described on [separate page of this documentation](configlink.md). 

Enhanced Iframe Manager integration
-----------------------------------

Since version 4.3.0 you can easily connect custom cookies category with Iframe Manager. It means, 
that your visitors could see settings dialog looking for example something like this:

And when they accept "Iframes" category (name it however you want), all iframes (or iframes of specific provider)
will be loaded automatically, as if user clicks the "Always load" button.

To achieve this, you have to follow these simple steps:

1.  Define Custom cookies block, Give it some title, description, alias for example as follows:
    ![Iframes custom category block](../images/usage/iframe-manager-enhanced.jpg)
2.  Add n3t_cc_ifm cookie definition to this block (this is not required, as the cookie itself could be still handled 
as Functional, or Preferences cookie, based on your desicion).

3. Add following script to Accept and Change events in n3t Cookie Consent settings
```  
n3tConsentManager.allowedCategory('iframes') ? n3tConsentManager.iframeManager.acceptService('all') : n3tConsentManager.iframeManager.rejectService('all');  
```

If you wish to allow only specific provider, lets say YouTube, modify the code as follows:

```  
n3tConsentManager.allowedCategory('iframes') ? n3tConsentManager.iframeManager.acceptService('youtube') : n3tConsentManager.iframeManager.rejectService('youtube');  
```

This way you could define one category per provider and give your visitors complete control over what is loaded and what is not.
