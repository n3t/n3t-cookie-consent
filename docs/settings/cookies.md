Cookies
=======

Cookies blocks definition
--------------------------

### Predefined blocks

n3t Cookie Consent comes with few predefined blocks:

- __Description__ - displays basic description about cookies
- __Functional cookies__ - these are cookies always enabled
- __Preferences cookies__ - cookies to store user preferences, such as selected language etc.
- __Analytics cookies__ - cookies for analytics, such as Google Analytics, Google Tag Manager etc.
- __Marketing cookies__ - cookies for retargeting, remarketing, such as Facebook remarketing, Google Ads etc.
- __Unknown cookies__ - block to hold automatically detected cookies, which were not automatically described
- __System cookies__ - same as 'Functional Cookies', but this block is not displayed in Settings dialog
- __Hidden cookies__ - same as 'Unknown Cookies', but this block is not displayed in Settings dialog (primarily 
  used for detected unknown cookies)
- __Privacy policy__ - Displays link to Privacy policy (it is displayed, only if Privacy policy link is filled)
- __Consent info__ - Displays info about user consent, date of consent and ID of consent

### Custom blocks

However, if you need more blocks (and cookies categories), or you want to specify different behavior for 
these blocks, there are two additional type of blocks:

- __Custom description block__ - displays any text with optional heading
- __Custom cookies block__ - displays cookies category. You are free to specify all the parameters for this category.

### Cookies parameters

Depending on block type and your site configuration, for every block you can define following parameters:

#### Title

Available for `Custom description` and `Custom cookies` blocks. Here you can specify Title of the block, how it will 
appear in consent dialog.

#### Description

Available for `Custom description` and `Custom cookies` blocks. Here you can specify Description of the block, how it will
appear in consent dialog.

!!! note "Multilanguage sites"
    If your site is set as multilingual, use for Title and Description in your custom blocks Language constants.
    You can define them in Joomla Language overrides dialog. Find more info in [Multilingual](multilanguage.md) documentation.

#### Enabled by default

Available for `Custom cookies` blocks. When Cookies blocks is enabled by default, cookies specified in it will not be blocked 
before user choice. This should be used only for functional Cookies, those necessary for site function (such as session cookies etc.)

#### Read-only

Available for `Custom cookies` blocks. When Cookies blocks is read-only, user cannot change its status. This makes sense only 
when `Enabled by default` option is also set for this block. It should be used for functional cookies blocks.

#### Cookies list

In the cookies list you can specify list of cookies in the category. This option is not available for `Description`,
`Privacy policy`, `Consent info` and `Custom description` blocks, as those doesn't contain any cookies.

![Cookies](../images/settings/cookies.jpg)

For every Cookie you can specify multiple parameters, depending on your site and n3t Cookie Consent configuration:

##### Name

This option specifies name of the cookie (not the name dissplayed to user, but the technical name of the cookie, such as n3t_cc etc.).
It could be simple name, or regular expression. When Regular expression is used, the name of the cookie is checked against this with Regular Expression. 
Enter regiular Expressions without any modifiers and initial and final slash (/). For example `^ga_` for all cookies atrting with `ga_`.

##### Description

This is the text, presented to user. It should contain short description of the cookie and its purpose. Remeber, that users will 
enable / disable Cookies on your site based on this description, so keep it short and descriptive.

##### Is RegExp

If name entered in `Name` column is Regular Expression, set this to `Yes`, otherwise leave it `No`. 

##### Provider

Provider of the Cookie, which service use this Cookie. This column could be disabled in [Settings dialog](appearance.md)
appearance settings. This should be something like Google, Youtube, or name of your site.

##### Expiration

Expiration of the Cookie, how long will this cookie remains on user computer. This column could be disabled in [Settings dialog](appearance.md)
appearance settings.

##### Expiration unit

Unit of expiration of the Cookie. This column could be disabled in [Settings dialog](appearance.md) appearance settings,
and will appear only if your site is set as multilingual.

!!! note "Multilanguage sites"
    If your site is set as multilingual, use for Description,  in your custom blocks Language constants.
    You can define them in Joomla Language overrides dialog. Find more info in [Multilingual](multilanguage.md) documentation.

