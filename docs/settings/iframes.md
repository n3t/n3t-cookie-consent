IFrames manager
===============

### Use IFrameManager

If this is disabled, iFrame manager will not load at all. 

### Enable YouTube privacy-enhanced mode

When enabled, YouTube video iframes URLs will be replaced with youtube-nocookie.com domain, 
which should bring more privacy to you users.

### Default thumbnail

Select default thumbnail for iframes, not specified in custom iframes.

### Custom iFrames providers

Specify providers of iFrames on your website, including default image to show instead of iFrame and 
link to providers Terms & Conditions. 

As Url specify just beginning of iFrame URl, including http(s):// at the beginning,
for example `https://www.google.com/maps/` for Google Maps.

!!! note "Built-in providers"
    There is built-in support for YouTube.com, Vimeo.com and DailyMotion.com video providers, 
    showing video thumbnail instead of default thumbnail. If you do not want to use this functionality, 
    just specify those providers in Custom providers list, adding you custom thumbnail to it.

### Whitelist iFrames

Here you can specify white list of URLs, that will not be handled by iFrame Manager.
Always specify just beginning of iFrame URl, including http(s):// at the beginning, 
for example `https://www.google.com/maps/` for Google Maps.
