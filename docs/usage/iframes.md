IFrame manager
==============

If you decide to use built-in iframe manager, rendered iframes are being blocked 
and attractive placeholder is shown on their place. User has option to accept conditions 
of given iframe provider, and display its content, or ignore it. 

So for example, instead of YouTube video, placeholder like this will be displayed:
![YoutUbe iframe](../images/usage/iframe-youtube.jpg)

User has option to load this specific content or allow to load all YouTube videos
on your site by default. Also link to Terms & Conditions of given provider is present.

Built-in providers
------------------

Following providers have built-in support to display video preview instead of video itself:

- YouTube.com
- Vime.com
- DailyMotion.com

Other providers
---------------

For any other provider just go to [Iframes configuration](../settings/iframes.md) and specify its parameters.

!!! note "Lazy loaded iframes"
    Note, that iFrame Manager is currently working for iFrames generated on server side. If iFrame is added 
    by JavaScript (for example some lazy loading script), iFrame manager will not work on such iFrame.


