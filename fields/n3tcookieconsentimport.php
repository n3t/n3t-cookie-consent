<?php
/**
 * @package n3t Cookie Consent
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2021- 2025 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

use Joomla\CMS\Form\FormField;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\Router\Route;

defined( '_JEXEC' ) or die( 'Restricted access' );

class JFormFieldN3tCookieConsentImport extends FormField
{
	protected $type = 'N3tCookieConsentImport';

	protected function getInput()
	{
		if (PluginHelper::isEnabled('system', 'n3tcookieconsent'))
			return
				'<input type="file" name="n3tcc[import]" accept="application/json" />' .
				'<button type="submit" class="btn btn-primary" formaction="' . Route::_('index.php?option=com_ajax&group=system&format=raw&plugin=N3tCookieConsentImport') . '" formenctype="multipart/form-data" formmethod="post">' .
				'<i class="icon-upload"></i> ' . Text::_('PLG_SYSTEM_N3TCOOKIECONSENT_CFG_IMPORT_BTN') .
				'</button>';

		return '<p class="alert alert-warning">' . Text::_('PLG_SYSTEM_N3TCOOKIECONSENT_CFG_ENABLE_PLUGIN_FIRST') . '</p>';
	}
}
