<?php
/**
 * @package n3t Cookie Consent
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2021 - 2025 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;

defined( '_JEXEC' ) or die( 'Restricted access' );
/**
 * @var array $displayData
 * @var array $options
 * @var array $iframeManagerOptions
 * @var string $trigger
 * @var array $cookies
 * @var bool $isScanMode
 * @var \Joomla\Registry\Registry $params
 * @var \Joomla\Registry\Registry $appConfig
**/
extract($displayData);
$appConfig = Factory::getConfig();
?>
<?php if (false) { ?><script><?php } // Just to make code highlighting working correctly ?>
	<?php echo $params->get('script_initialize'); ?>
	<?php if ($params->get('script_loaded')) { ?>
  window.addEventListener('n3t.cookieconsent.loaded', function(event) {
		<?php echo $params->get('script_loaded'); ?>
  });
	<?php } ?>
	<?php if ($params->get('script_first_action')) { ?>
  window.addEventListener('n3t.cookieconsent.first_action', function(event) {
		<?php echo $params->get('script_first_action'); ?>
  });
	<?php } ?>
	<?php if ($params->get('script_accept')) { ?>
  window.addEventListener('n3t.cookieconsent.accept', function(event) {
		<?php echo $params->get('script_accept'); ?>
  });
	<?php } ?>
	<?php if ($params->get('script_change')) { ?>
  window.addEventListener('n3t.cookieconsent.change', function(event) {
		<?php echo $params->get('script_change'); ?>
  });
	<?php } ?>
  initN3tConsentManager({
	  <?php if (JDEBUG) { ?>
    debug: true,
	  <?php } ?>
    cookie_name: <?php echo json_encode($params->get('cookie_name', 'n3t_cc')); ?>,
	  <?php if ($isScanMode) { ?>
    cookie_report_name: <?php echo json_encode($params->get('cookie_name', 'n3t_cc') . '_report'); ?>,
	  <?php if ($path = $params->get('cookie_path', $appConfig->get('cookie_path'))) { ?>
    cookie_path: <?php echo json_encode($path); ?>,
	  <?php } ?>
	  <?php if ($domain = $params->get('cookie_domain', $appConfig->get('cookie_domain'))) { ?>
    cookie_domain: <?php echo json_encode($domain); ?>,
	  <?php } ?>
	  <?php } ?>
    <?php if ($params->get('consent_reload')) { ?>
	  consent_reload: true,
    <?php } ?>
	  <?php
      if ($params->get('cookie_domains')) {
        $domains = [];
        foreach ($params->get('cookie_domains') as $domain)
          $domains[] = rtrim($domain->domain, '/') . '/index.php?option=com_ajax&group=system&format=raw&plugin=N3tCookieConsentSetCookie'
    ?>
    cookie_domains: <?php echo json_encode($domains); ?>,
	  <?php } ?>
    cookies: <?php echo json_encode($cookies); ?>,
    options: <?php echo json_encode($options); ?>,
    unknown_cookies: <?php echo json_encode($params->get('allow_unknown_cookies', 'settings')); ?>,
	  <?php if ($trigger) { ?>
    show_trigger: true,
    trigger_text: <?php echo json_encode(Text::_('PLG_SYSTEM_N3TCOOKIECONSENT_TRIGGER')); ?>,
    trigger_icon: <?php echo json_encode($trigger); ?>,
    trigger_layout: <?php echo json_encode($params->get('trigger_layout', 'icon')); ?>,
    trigger_position: <?php echo json_encode($params->get('trigger_position', 'bottom left')); ?>,
	  <?php } ?>
	  <?php if ($params->get('tertiary_button_role', 'none') != 'none') { ?>
    tertiary_button_role: <?php echo json_encode($params->get('tertiary_button_role', 'none')); ?>,
    tertiary_button_text: <?php echo json_encode(Text::_('PLG_SYSTEM_N3TCOOKIECONSENT_BTN_' . $params->get('tertiary_button_role'))); ?>,
    tertiary_button_position: <?php echo json_encode($params->get('consent_modal_swap_buttons', true) ? 'first' : 'last'); ?>,
	  <?php } ?>
    log_user_preferences_url: <?php echo json_encode(Route::_('index.php?option=com_ajax&group=system&format=json&plugin=N3tCookieConsentLogConsent', false)); ?>,
	  <?php if ($params->get('use_iframe_manager', false)) { ?>
    use_iframemanager: true,
    iframemanager_options: <?php echo json_encode($iframeManagerOptions); ?>,
	  <?php } ?>
  });
<?php if (false) { ?></script><?php } // Just to make code highlighting working correctly
